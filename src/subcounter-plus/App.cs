﻿using Newtonsoft.Json;
using System;
using Bridge;
using Retyped;
using Platform = Retyped.node.NodeJS.Platform;
using lit = Retyped.electron.Literals;

namespace subcounter_plus
{
    public class App
    {
        [Init(InitPosition.Top)]
        public static void InitGlobals()
        {
            node.require.Self("./bridge.js");

            // The call below is required to initialize a global var 'Electron'.
            var Electron = (electron.Electron.AllElectron)node.require.Self("electron");

            // Keep a global reference of the window object, if you don't, the window will
            // be closed automatically when the JavaScript object is garbage collected.
            electron.Electron.BrowserWindow win = null;
        }


        [Template("Electron")]
        public static electron.Electron.AllElectron Electron;

        [Template("win")]
        public static electron.Electron.BrowserWindow Win;

        public static void Main()
        {
            var app = Electron.app;

            // This method will be called when Electron has finished
            // initialization and is ready to create browser windows.
            // Some APIs can only be used after this event occurs.
            app.on(lit.ready, StartApp);

            // Quit when all windows are closed.
            app.on(lit.window_all_closed, () =>
            {
                    app.quit();
            });


        }

        private static void StartApp(object launchInfo)
        {
            CreateMainWindow();

            Win.once(lit.ready_to_show, () =>
            {
                Win.show();
                Win.focus();
            });

            
        }

        private static void CreateMainWindow()
        {
            var options = ObjectLiteral.Create<electron.Electron.BrowserWindowConstructorOptions>();
            options.width = 600;
            options.height = 800;
            options.title = "ven1xxy's subscriber counter";
            options.show = false;

            // Create the browser window.
            Win = new electron.Electron.BrowserWindow(options);

            App.LoadWindow(Win, "forms/MainForm.html");

            Win.on(lit.closed, () =>
            {
                // Dereference the window object, usually you would store windows
                // in an array if your app supports multi windows, this is the time
                // when you should delete the corresponding element.
                Win = null;
            });
        }

        private static void LoadWindow(electron.Electron.BrowserWindow win, string page)
        {
            var windowUrl = ObjectLiteral.Create<node.url.URL>();
            windowUrl.pathname = node.path.join(node.__dirname, page);
            windowUrl.protocol = "file:";

            var formattedUrl = node.url.format(windowUrl);

            // and load the index.html of the app.
            win.loadURL(formattedUrl);
        }

    }
}