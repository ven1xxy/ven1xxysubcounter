Bridge.assembly("subcounter-plus", function ($asm, globals) {
    "use strict";


    var $m = Bridge.setMetadata,
        $n = ["System"];
    $m("subcounter_plus.App", function () { return {"att":1048577,"a":2,"m":[{"a":2,"isSynthetic":true,"n":".ctor","t":1,"sn":"ctor"},{"a":1,"n":"CreateMainWindow","is":true,"t":8,"sn":"CreateMainWindow","rt":$n[0].Void},{"a":2,"n":"InitGlobals","is":true,"t":8,"sn":"InitGlobals","rt":$n[0].Void},{"a":1,"n":"LoadWindow","is":true,"t":8,"pi":[{"n":"win","pt":Electron.BrowserWindow,"ps":0},{"n":"page","pt":$n[0].String,"ps":1}],"sn":"LoadWindow","rt":$n[0].Void,"p":[Electron.BrowserWindow,$n[0].String]},{"a":2,"n":"Main","is":true,"t":8,"sn":"Main","rt":$n[0].Void},{"a":1,"n":"StartApp","is":true,"t":8,"pi":[{"n":"launchInfo","pt":$n[0].Object,"ps":0}],"sn":"StartApp","rt":$n[0].Void,"p":[$n[0].Object]},{"a":2,"n":"Electron","is":true,"t":4,"rt":System.Object,"sn":"Electron"},{"a":2,"n":"Win","is":true,"t":4,"rt":Electron.BrowserWindow,"sn":"Win"}]}; }, $n);
});
