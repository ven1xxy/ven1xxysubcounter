require("./bridge.js");

// The call below is required to initialize a global var 'Electron'.
var Electron = require("electron");

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var win = null;

/**
 * @version 1.0.0.0
 * @copyright Copyright ©  2018
 * @compiler Bridge.NET 17.5.0
 */
Bridge.assembly("subcounter-plus", function ($asm, globals) {
    "use strict";

    var Electron = require("electron");
    var path = require("path");
    var url = require("url");

    Bridge.define("subcounter_plus.App", {
        main: function Main () {
            var app = Electron.app;

            // This method will be called when Electron has finished
            // initialization and is ready to create browser windows.
            // Some APIs can only be used after this event occurs.
            app.on("ready", subcounter_plus.App.StartApp);

            // Quit when all windows are closed.
            app.on("window-all-closed", function () {
                app.quit();
            });


        },
        statics: {
            fields: {
                Electron: null,
                Win: null
            },
            methods: {
                StartApp: function (launchInfo) {
                    subcounter_plus.App.CreateMainWindow();

                    win.once("ready-to-show", function () {
                        win.show();
                        win.focus();
                    });


                },
                CreateMainWindow: function () {
                    var options = { };
                    options.width = 600;
                    options.height = 800;
                    options.title = "ven1xxy's subscriber counter";
                    options.show = false;

                    // Create the browser window.
                    win = new Electron.BrowserWindow(options);

                    subcounter_plus.App.LoadWindow(win, "forms/MainForm.html");

                    win.on("closed", function () {
                        // Dereference the window object, usually you would store windows
                        // in an array if your app supports multi windows, this is the time
                        // when you should delete the corresponding element.
                        win = null;
                    });
                },
                LoadWindow: function (win, page) {
                    var windowUrl = { };
                    windowUrl.pathname = path.join(__dirname, page);
                    windowUrl.protocol = "file:";

                    var formattedUrl = url.format(windowUrl);

                    // and load the index.html of the app.
                    win.loadURL(formattedUrl);
                }
            }
        }
    });
});
